﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using System;
using System.Collections.Concurrent;
using SignalR_GameServer_v1.Models;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace SignalR_GameServer_v1.Hubs
{
    public class GameHub : Hub
    {
        private static List<Player> _players = new List<Player>();
        private static Game _game = new Game();
        public static IHubContext<GameHub> _hubContext;

        public GameHub()
        {
        }

        public override async Task OnConnectedAsync()
        {
            await base.OnConnectedAsync();
        }

        // TODO: Create MapObject class
        public async Task UpdateObstacles()
        {
            await Clients.All.SendAsync("UpdateObstacles", _game.Obstacles);
        }

        public async Task GetAllObstacles()
        {
            await Clients.All.SendAsync("GetAllObstacles", _game.Obstacles);
        }

        public async Task UpdateEnemies()
        {
            await Clients.All.SendAsync("UpdateEnemies", _game.Enemies);
        }

        public async Task UpdateBonuses()
        {
            await Clients.All.SendAsync("UpdateBonuses", _game.Bonuses);
        }

        public async Task UpdatePlanePos(int X, int Y)
        {
            _game.Planes.Find(x => x.Player.Id == Context.ConnectionId).SetPosXY(X,Y);
            await GetAllPlanes();
        }

        public async Task UpdateMissilePos(Missile missile)
        {
            _game.Planes.Find(x => x.Player.Id == Context.ConnectionId)
                .Missiles.Find(x => x == missile).SetMissile(missile);
            await GetAllPlanes();
        }

        public async Task UpdateMissiles(List<Missile> missiles)
        {
            _game.Planes.Find(x => x.Player.Id == Context.ConnectionId).SetMissiles(missiles);
            await GetAllPlanes();
        }

        public override async Task OnDisconnectedAsync(System.Exception exception)
        {
            var user = _players.FirstOrDefault(x => x.Id == Context.ConnectionId);
            if (user == null)
                return;

            await RemovePlayer();

            await base.OnDisconnectedAsync(exception);
        }

        public async Task AddUser(string name)
        {
            Player player;
            Plane plane;

            lock (_players)
            {
                name = Regex.Replace(name, @"\s+", "").ToLower();

                if (name.Length > 10)
                    name = name.Substring(0, 10);

                var nameExists = _players.Any(x => x.Name == name);
                if (nameExists)
                {
                    Random rnd = new Random();
                    name = name + rnd.Next(1, 100);
                }

                player = new Player(Context.ConnectionId, name);

                _players.Add(player);
                plane = SetPlaneColour();
                _game.Planes.Add(plane);
            }

            Console.WriteLine(plane.Player.Name);
            await Clients.Client(Context.ConnectionId).SendAsync("SetCurrentPlayer", plane);
            await GetAllPlayers();
            await base.OnConnectedAsync();
        }

        public async Task StartGame()
        {
            var player = _players.FirstOrDefault(x => x.Id == Context.ConnectionId);
            if (player == null) return;

            var success = _game.StartGame();

            if (success)
                await UpdateGame(_game);

            await UpdateGame(_game);
        }

        private async Task UpdateGame(Game game)
        {
            await Clients.All.SendAsync("UpdateGame", game);
        }

        public async Task ExitGame()
        {
            if (_game == null)
                return;

            var player = _game.Planes.FirstOrDefault(x => x.Player.Id == Context.ConnectionId);

            _game.Planes.Remove(player);
            if(_game.Planes.Count == 0)
            {
                _game.stopGenerating();
                _game = new Game();
            }

            await UpdateGame(_game);
        }

        public async Task GetAllPlayers()
        {
            await Clients.All.SendAsync("GetAllPlayers", _players);
        }

        public async Task GetAllPlanes()
        {
            await Clients.All.SendAsync("GetAllPlanes", _game.Planes);
        }

        public async Task TogglePlayerReady()
        {
            _players.Find(x => x.Id == Context.ConnectionId).IsReady = true;
            _game.ReadyCount++;

            await GetAllPlayers();
            if (_game.ReadyCount == _players.Count)
            {
                _game.StartGame();
                await Clients.All.SendAsync("StartGame", _game);
            }
        }

        public async Task RemoveEnemy(String uid)
        {
            await Clients.Others.SendAsync("removeEnemy", uid);
        }

        public async Task RemoveCrate(String uid)
        {
            await Clients.Others.SendAsync("removeObstacle", uid);
        }

        public async Task PlayerLostAllLives()
        {
            var player = _players.FirstOrDefault(x => x.Id == Context.ConnectionId);

            await Clients.All.SendAsync("playerLost", player.Name);
        }

        public async Task PlayerScored(int score)
        {
            var player = _players.FirstOrDefault(x => x.Id == Context.ConnectionId);
            player.Score += score;

            await Clients.All.SendAsync("playerScored", score, player.Name);
        }


        private async Task RemovePlayer()
        {
            var player = _players.FirstOrDefault(x => x.Id == Context.ConnectionId);

            if (player != null)
            {
                if (player.IsReady)
                {
                    _game.ReadyCount--;
                }
                _players.Remove(player);

            }

            await ExitGame();
            await GetAllPlanes();
        }

        private Plane SetPlaneColour()
        {
            List<string> colours = new List<string>()
            {
                "RED",
                "BLUE",
                "GREEN",
                "YELLOW"
            };
            var player = _players.FirstOrDefault(x => x.Id == Context.ConnectionId);
            Plane plane = new Plane(player);

            if (_game.Planes.Count == 0)
            {
                return plane;
            }

            var usedColours = _game.Planes.Select(x => x.Colour).ToList();
            var availableColours = colours.Except(usedColours).ToList();

            plane = new Plane(player, availableColours.First());
            plane.PosY = 170 * availableColours.Count();
            return plane;
        }
    }
}
