﻿using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using SignalR_GameServer_v1.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace SignalR_GameServer_v1.Models
{
    public class Game
    {
        private static List<String> OBSTACLES = new List<String>()
        {   "BLACKHOLE",
            "ROCK",
            "LIGHTNING",
            "METEORITE" 
        };

        private static List<String> BONUSES = new List<String>()
        {   "Movement",
            "Freeze",
            "Power",
            "Immunity"
        };
        public List<Plane> Planes { get; set; }
        public List<Obstacle> Obstacles { get; set; }
        public List<Enemy> Enemies { get; set; }

        public List<Bonus> Bonuses { get; set; }
        public int ReadyCount { get; set; } = 0;
        public bool GameEnded { get; set; } = false;
        public bool GameStarted { get; set; } = false;
        public bool GamePaused { get; set; } = false;

        int random = new Random().Next(0, 30);


        [JsonIgnore]
        private readonly TimeSpan _updateInterval = TimeSpan.FromMilliseconds(16);

        [JsonIgnore]
        private readonly TimeSpan _updateIntervalGenerate = TimeSpan.FromMilliseconds(3500);

        [JsonIgnore]
        private readonly TimeSpan _updateIntervalEnemies = TimeSpan.FromMilliseconds(16);

        [JsonIgnore]
        private readonly TimeSpan _updateIntervalGenerateEnemies = TimeSpan.FromMilliseconds(2000);

        [JsonIgnore]
        private DateTime _startTime;

        [JsonIgnore]
        private readonly Random _updateOrNotRandom = new Random();

        [JsonIgnore]
        private Timer _timer;
        [JsonIgnore]
        private Timer _timerGenerateObstacle;
        [JsonIgnore]
        private Timer _timerGenerateEnemies;

        [JsonIgnore]
        TimeSpan gameTime;

        public Game()
        {
            Planes = new List<Plane>();
            Obstacles = new List<Obstacle>();
            Bonuses = new List<Bonus>();
            Enemies = new List<Enemy>();
        }

        public bool StartGame()
        {
            GameStarted = true;
            _timerGenerateEnemies = new Timer(GenerateEnemies, null, _updateIntervalEnemies, _updateIntervalGenerateEnemies);
            _timerGenerateObstacle = new Timer(GenerateObstacle, null, _updateIntervalGenerate, _updateIntervalGenerate);
            _timer = new Timer(UpdateGame, null, _updateInterval, _updateInterval);
            _startTime = DateTime.Now;
            return GameStarted;
        }

        private void UpdateGame(Object state)
        {
            gameTime = DateTime.Now - _startTime;
            int time = (int)gameTime.TotalSeconds;
            if (time > random)
            {
                int randomObsY = new Random().Next(100, 500);
                GameHub._hubContext.Clients.All.SendAsync("generateBonus", randomBonus(), randomObsY, Guid.NewGuid());
                random = new Random().Next(time, time + 30);
            }
            GameHub._hubContext.Clients.All.SendAsync("updateTimer", (int) gameTime.TotalSeconds);
            GameHub._hubContext.Clients.All.SendAsync("updateObstacles");
            GameHub._hubContext.Clients.All.SendAsync("updateCrates");
            GameHub._hubContext.Clients.All.SendAsync("updateEnemies");
        }

        private string randomBonus()
        {
            int randomObs = new Random().Next(0, 3);
            return BONUSES.ElementAt(randomObs);
        }

        private void GenerateObstacle(Object state)
        {
            int randomObs = new Random().Next(0, 3);
            int randomObsY = new Random().Next(100, 500);
            Obstacle obs = new Obstacle(700, randomObsY, 0, 0);
            
            GameHub._hubContext.Clients.All.SendAsync("generateObstacle", OBSTACLES.ElementAt(randomObs), randomObsY, obs.ID);
        }

        private void GenerateEnemies(Object state)
        {
            int randomObs = new Random().Next(0, 3);
            int randomObsY = new Random().Next(100, 500);

            if (gameTime.Minutes % 2 == 0)
            {
                GameHub._hubContext.Clients.All.SendAsync("generateEnemies", "SpaceShip", randomObsY, Guid.NewGuid(), true);
            }
            else 
            {
                GameHub._hubContext.Clients.All.SendAsync("generateEnemies", "SpaceShip", randomObsY, Guid.NewGuid(), false);
            }
        }

        public void stopGenerating()
        {
            _timer.Dispose();
            _timerGenerateObstacle.Dispose();
            _timerGenerateEnemies.Dispose();
        }

        public void SetObstacles(List<Obstacle> obstacles)
        {
            Obstacles = obstacles;
        }
    }
}
