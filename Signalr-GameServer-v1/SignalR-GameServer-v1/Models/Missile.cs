﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR_GameServer_v1.Models
{
    public class Missile
    {
        public int PosX { get; set; }
        public int PosY { get; set; }

        public Missile()
        {

        }

        public void SetMissile(Missile mis)
        {
            PosX = mis.PosX;
            PosY = mis.PosY;
        }
    }
}
