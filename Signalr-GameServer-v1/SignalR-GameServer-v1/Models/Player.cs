﻿using SignalR_GameServer_v1.Observer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace SignalR_GameServer_v1.Models
{
    public class Player
    {
        //Connection id
        [JsonIgnore]
        public string Id { get; set; }
        public string Name { get; set; }
        public long Score { get; set; }
        //Is player ready in lobby
        public bool IsReady { get; set; }
        public int Lives { get; set; }

        public List<Bonus> ActiveBonuses { get; set; }

        public Player()
        {
        }

        public Player(string connectionId, string name)
        {
            Id = connectionId;
            Name = name;
            Score = 0;
            IsReady = false;
            Lives = 3;
        }
    }
}
