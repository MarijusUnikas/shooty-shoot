﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR_GameServer_v1.Models
{
    public class Bonus
    {
        public int PosX { get; set; }
        public int PosY { get; set; }
        
        public Bonus()
        {

        }
    }
}
