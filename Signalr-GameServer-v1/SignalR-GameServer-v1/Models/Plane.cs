﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR_GameServer_v1.Models
{
    public class Plane
    {
        public int PosX { get; set; }
        public int PosY { get; set; }
        //Health Points
        public int HP { get; set; }
        public string Colour { get; set; }
        public Player Player { get; set; }

        public List<Missile> Missiles { get; set; }

        public Plane(Player player)
        {
            Player = player;
            PosX = 50;
            PosY = 50;
            HP = 100;
            Colour = "RED";
            Missiles = new List<Missile>();
        }

        public Plane(Player player, string colour)
        {
            Player = player;
            PosX = 50;
            PosY = 200;
            HP = 100;
            Colour = colour;
            Missiles = new List<Missile>();
        }

        public void SetPosXY(int x, int y)
        {
            PosX = x;
            PosY = y;
        }

        public void SetMissiles(List<Missile> missiles)
        {
            Missiles = missiles;
        }
    }
}
