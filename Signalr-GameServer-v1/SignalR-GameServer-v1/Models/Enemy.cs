﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR_GameServer_v1.Models
{
    public class Enemy
    {
        public int PosX { get; set; }
        public int PosY { get; set; }
        public int GivenPoints { get; set; }
        //Health Points
        public int HP { get; set; }
        public Guid ID { get; set; }

        public Enemy(int x, int y, int hp, int givenPoints)
        {
            PosX = x;
            PosY = y;
            HP = hp;
            GivenPoints = givenPoints;
            ID = Guid.NewGuid();
        }
    }
}
