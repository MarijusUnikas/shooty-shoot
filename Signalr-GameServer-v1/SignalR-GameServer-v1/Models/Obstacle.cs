﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR_GameServer_v1.Models
{
    public class Obstacle
    {
        public int PosX { get; set; }
        public int PosY { get; set; }
        [JsonIgnore]
        public int HP { get; set; }
        [JsonIgnore]
        public int GivenPoints { get; set; }
        public Guid ID { get; set; }

        public Obstacle()
        {
       
        }

        public Obstacle(int x, int y, int hp, int givenPoints)
        {
            PosX = x;
            PosY = y;
            HP = hp;
            GivenPoints = givenPoints;
            ID = Guid.NewGuid();
        }

        public void SetObstacle(Obstacle obs)
        {
            PosX = obs.PosX;
            PosY = obs.PosY;
            HP = obs.HP;
            GivenPoints = obs.GivenPoints;
        }

        public void SetObstacle(int x, int y, int hp, int givenPoints)
        {
            PosX = x;
            PosY = y;
            HP = hp;
            GivenPoints = givenPoints;
        }
    }
}
