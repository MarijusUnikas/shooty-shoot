﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR_GameServer_v1.Observer
{
    interface ISubject
    {
        void Attach(IObserver observer);

        void Detach(IObserver observer);

        // Notify all observers
        void Notify();
    }
}
