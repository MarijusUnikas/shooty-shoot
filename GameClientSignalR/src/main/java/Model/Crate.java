package main.java.Model;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

import static main.java.util.Util.getRandomNumber;

public class Crate {

    @com.google.gson.annotations.SerializedName("PosX")
    private int x;
    @com.google.gson.annotations.SerializedName("PosY")
    private int y;
    private transient String id;

    private String bonusName;

    private transient Image image;
    protected transient boolean visible;

    private final int INITIAL_X = 700;

    public Crate(String bonusName, int y){
        x = INITIAL_X;
        this.y = y;
        visible = true;
        this.bonusName = bonusName;
        loadImage();
    }

    public void loadImage() {
        ClassLoader cl = getClass().getClassLoader();
        URL pathToImage = cl.getResource("main/res/crate.png");
        ImageIcon ii = new ImageIcon(pathToImage);
        image = ii.getImage();
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public int getX() {

        return x;
    }

    public int getY() {
        return y;
    }

    public Image getImage() {

        return image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, 15, 15);
    }

    public String getBonusName() {
        return bonusName;
    }

    public void move(){
        x -= 1;

        if(x < 0){
            setVisible(false);
        }
    }
}
