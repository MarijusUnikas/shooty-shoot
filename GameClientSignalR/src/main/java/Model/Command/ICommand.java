package main.java.Model.Command;

public interface ICommand {
    public abstract void execute();
    public abstract void undo();

}
