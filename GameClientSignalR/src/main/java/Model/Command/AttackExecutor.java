package main.java.Model.Command;

import java.util.ArrayList;
import java.util.List;

public class AttackExecutor {
    private final List<ICommand> attackCommands = new ArrayList<>();
    public void executeOperation(ICommand attackCommand){
        attackCommands.add(attackCommand);
        attackCommand.execute();
    }
    public void undoOperation(){
        if (attackCommands.size() <= 0){
            return;
        }
        attackCommands.get(attackCommands.size()-1).undo();
    }
}
