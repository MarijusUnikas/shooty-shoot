package main.java.Model.Command;

import main.java.Model.AbstractFactory.Enemy;

public class AttackCommand implements ICommand {
    private Enemy enemy;

    public AttackCommand(Enemy enemy){
        this.enemy = enemy;
    }
    @Override
    public void execute() {
        enemy.stopAttacking();
    }

    @Override
    public void undo() {
        enemy.startAttacking();
    }
}
