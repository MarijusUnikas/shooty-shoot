package main.java.Model.Bridge;

import java.awt.*;

public interface MoveAlgorithm {
    public Point move(Point xy);
}
