package main.java.Model.Bridge;

import java.awt.*;

public class Straight implements MoveAlgorithm{
    @Override
    public Point move(Point xy) {
        xy.x -= 1;
        return xy;
    }
}
