package main.java.Model.Bridge;

import java.awt.*;
import java.util.Random;

public class Zigzag implements MoveAlgorithm{

    boolean upWards = false;

    public Zigzag(){
    }

    @Override
    public Point move(Point xy) {
        xy.x--;
        if(!upWards) {
            xy.y++;
        }
        else xy.y--;

        if(xy.y < 0)
            upWards = false;
        if(xy.y > 600)
            upWards = true;

        return xy;
    }
}
