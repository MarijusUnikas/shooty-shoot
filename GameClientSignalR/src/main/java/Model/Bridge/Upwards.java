package main.java.Model.Bridge;
import java.awt.Point;


public class Upwards implements MoveAlgorithm {

    public Upwards(){
    }
    
    @Override
    public Point move(Point xy) {
        if(xy.y <= 0){
            xy.y = 550;
        }
        else xy.y -= 1;
        return xy;
    }
}
