package main.java.Model.AbstractFactory;

import javax.swing.*;
import java.awt.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.java.Model.Bridge.MoveAlgorithm;
import main.java.Model.Bridge.Straight;
import main.java.Model.Bridge.Upwards;
import main.java.Model.Bridge.Zigzag;

public class Enemy {

    private final int INITIAL_X = 700;
    private transient String id;
    @com.google.gson.annotations.SerializedName("PosX")
    private int x;
    @com.google.gson.annotations.SerializedName("PosY")
    private int y;
    @com.google.gson.annotations.SerializedName("HP")
    private int hp;

    private transient int width;
    private transient int height;
    private transient Image image;
    protected transient boolean visible;
    private ArrayList<MoveAlgorithm> moveAlgorithms;


    public Enemy(){
        x = INITIAL_X;
        y = getRandomNumber(50, 550);
        hp = 100;
        visible = true;
    }

    public void loadImage(String imgName) {
        ClassLoader cl = getClass().getClassLoader();
        URL pathToImage = cl.getResource("main/res/"+imgName+".png");
        ImageIcon ii = new ImageIcon(pathToImage);
        image = ii.getImage();

        width = image.getWidth(null);
        height = image.getHeight(null);
    }

    public void move() {
        Point temp = new Point(x, y);

        if(hp > 60) {
            temp = moveAlgorithms.get(1).move(temp);
        } else if(hp <= 60 && hp > 30) {
            temp = moveAlgorithms.get(2).move(temp);
        } else {            
            temp = moveAlgorithms.get(0).move(temp);            
        }
        
        x = temp.x;
        y = temp.y;

        if(x < 0) {
            visible = false;
        }
    }

    public boolean isVisible() {
        return visible;
    }

    public void setMoveAlgorithms(ArrayList<MoveAlgorithm> moveAlgorithms) {
        this.moveAlgorithms = moveAlgorithms;        
    }
    
    public void setVisible(Boolean visible) {
        this.visible = visible;
    }
    
    public void gotHit(int dmg) {
        hp = hp - dmg;
    }

    public int getX() {

        return x;
    }

    public int getY() {
        return y;
    }
    
    public int getHp() {
        return hp;
    }

    public void setY(int posY) {
        y = posY;
    }


    public Image getImage() {

        return image;
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, 25, 25);
    }

    public int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void stopAttacking() {
        System.out.println("Stopping attacking now");
    }

    public void startAttacking(){
        System.out.println("Starting attacking now");
    }
}
