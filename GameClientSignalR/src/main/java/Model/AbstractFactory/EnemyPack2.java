package main.java.Model.AbstractFactory;

public class EnemyPack2 extends EnemyAbstractFactory{

    @Override
    public Bird createBird() {
        return new Stork();
    }

    @Override
    public SpaceShip createSpaceShip() {
        return new Drone();
    }

    @Override
    public Boss createBoss() {
        return new PowerfulBoss();
    }
}
