package main.java.Model.AbstractFactory;

public class EnemyPack1 extends EnemyAbstractFactory{

    @Override
    public Bird createBird() {
        return new Eagle();
    }

    @Override
    public SpaceShip createSpaceShip() {
        return new Alien();
    }

    @Override
    public Boss createBoss() {
        return new AggressiveBoss();
    }
}
