package main.java.Model.AbstractFactory;

public abstract class EnemyAbstractFactory {

    public abstract Bird createBird();

    public abstract SpaceShip createSpaceShip();

    public abstract Boss createBoss();
}
