package main.java.Model.AbstractFactory;

import java.util.ArrayList;
import main.java.Model.Bridge.MoveAlgorithm;
import main.java.Model.Bridge.Straight;
import main.java.Model.Bridge.Upwards;
import main.java.Model.Bridge.Zigzag;

public class Alien extends SpaceShip {

    public Alien() {
        loadImage("Spaceship_02_GREEN-50_x_50");
        super.setMoveAlgorithms(MovementAlgorithms());
    }
    
    private ArrayList<MoveAlgorithm> MovementAlgorithms(){
        ArrayList<MoveAlgorithm> algorithms = new ArrayList<MoveAlgorithm>();
        algorithms.add(new Straight());
        algorithms.add(new Zigzag()); 
        algorithms.add((MoveAlgorithm) new Upwards());
        return algorithms;
    }
    
    @Override
    public void loadImage(String imageName) {
        super.loadImage(imageName);


    }
}
