package main.java.Model.AbstractFactory;

import java.util.ArrayList;
import main.java.Model.Bridge.MoveAlgorithm;
import main.java.Model.Bridge.Straight;
import main.java.Model.Bridge.Upwards;
import main.java.Model.Bridge.Zigzag;


public class Drone extends SpaceShip {

    public Drone() {
        loadImage("Spaceship_06_BLUE-50_x_50");
        super.setMoveAlgorithms(MovementAlgorithms());
    }

    private ArrayList<MoveAlgorithm> MovementAlgorithms(){
        ArrayList<MoveAlgorithm> algorithms = new ArrayList<MoveAlgorithm>();
        algorithms.add(new Straight());
        algorithms.add((MoveAlgorithm) new Upwards());
        algorithms.add(new Zigzag()); 

        return algorithms;
    }
    
    @Override
    public void loadImage(String imageName) {
        super.loadImage(imageName);

    }
}
