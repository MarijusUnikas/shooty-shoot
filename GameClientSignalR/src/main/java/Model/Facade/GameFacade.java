/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.Model.Facade;

import main.java.Model.Plane;

public interface GameFacade
{
    Plane init( );
	void removeEnemy( String uid );
	void generateEnemies( String enemyType, int posY, String uid, boolean isFirstPack );
	void generateObstacle( String name, int posY, String uid );
	void playerScored(int scored, String playerName);
	void playerLost(String playerName);
	
	
}
