package main.java.Model.Strategy;
import javax.swing.*;
import java.awt.*;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.java.Model.Prototype.IPrototype;

//TODO: Create MapOBJECT
public class Weapon implements IPrototype, Cloneable {
    @com.google.gson.annotations.SerializedName("PosX")
    private int x;
    @com.google.gson.annotations.SerializedName("PosY")
    private int y;
    @com.google.gson.annotations.SerializedName("Damage")
    private int damage;    
    @com.google.gson.annotations.SerializedName("Speed")
    private int speed;        
    @com.google.gson.annotations.SerializedName("ImgPath")
    private String imgPath;            
    
    protected transient boolean visible;
    private transient  int width;
    private transient  int height;
    private transient  Image image;
    private transient final int BOARD_WIDTH = 800;


    
    public Weapon(int x, int y, int dmg, int speed, String path) {
        this.x = x;
        this.y = y;
        this.damage = dmg;
        this.speed = speed;
        this.imgPath = path;

        visible = true;
        loadImage();
    }
    
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    
    public void setY(int y) {
       this.y = y;
    }

    public void setX(int x){
        this.x = x;
    }

    public String getImgPath() {
        return imgPath;
    }

    public int getDamage() {
        return damage;
    }
    

    
    public Rectangle getBounds() {
        return new Rectangle(x, y, 50, 50);
    }
        
    

    public void move() {

        x += speed;

        if (x > BOARD_WIDTH)
            visible = false;
    }
        
    public Image getImage() {
        return image;
    }

    public void loadImage() {
        ClassLoader cl = getClass().getClassLoader();
        URL pathToImage = cl.getResource("main/res/crate.png");
        ImageIcon ii = new ImageIcon(pathToImage);
        image = ii.getImage();

        width = image.getWidth(null);
        height = image.getHeight(null);
    }

            
    public boolean isVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    @Override
    public Object clone() {  
        try {  
            return super.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Weapon.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }  

}
