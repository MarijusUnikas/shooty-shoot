package main.java.Model.Strategy;

import java.util.ArrayList;
import java.util.List;

public class Bullet implements IWeapon{

    @Override
    public List<Missile> shoot(int x, int y) {
        List<Missile> missiles = new ArrayList<>();

        missiles.add(new Missile(x, y));
        return missiles;
    }
}
