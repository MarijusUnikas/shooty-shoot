package main.java.Model.Strategy;

import java.util.ArrayList;
import java.util.List;

public class Triangle implements IWeapon{


    @Override
    public List<Missile> shoot(int x, int y) {
        List<Missile> missiles = new ArrayList<>();

        Missile mis = new Missile(x, y);
        Missile clone = mis.clone();
        Missile clone2 = mis.clone();

        clone.setX(x+30);
        clone2.setY(y+20);
        mis.setY(y-20);

        missiles.add(clone);
        missiles.add(clone2);
        missiles.add(mis);

        return missiles;
    }
}
