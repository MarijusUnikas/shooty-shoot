package main.java.Model.Strategy;

import java.util.ArrayList;
import java.util.List;

public class Railgun implements IWeapon{

    @Override
    public List<Missile> shoot(int x, int y) {
        List<Missile> missiles = new ArrayList<>();
        Missile mis = new Missile(x, y);
        for(int i = 0; i < 7; i++)
        {
            Missile copy1 = mis.clone();
            for(int j = 0; j < i+1 && i != 3; j++)
            {
                copy1.setY(y+j*3);
                copy1.setX(x+i*2);
                missiles.add(copy1);
            }
        }

        return missiles;
    }
}
