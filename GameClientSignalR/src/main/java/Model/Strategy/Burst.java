package main.java.Model.Strategy;

import java.util.ArrayList;
import java.util.List;

public class Burst implements IWeapon{

    @Override
    public List<Missile> shoot(int x, int y) {
        List<Missile> missiles = new ArrayList<>();
        missiles.add(new Missile(x, y));
        missiles.add(new Missile(x+10, y));
        missiles.add(new Missile(x+20, y));
        return missiles;
    }
}
