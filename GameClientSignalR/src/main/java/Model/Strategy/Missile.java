package main.java.Model.Strategy;

import javax.swing.*;
import java.awt.*;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

//TODO: Create MapOBJECT
public class Missile extends Weapon {

    private final int MISSILE_SPEED = 3;
    private final int MISSILE_DAMAGE = 35;
    private final String IMG_PATH = "main/res/Flame_01-100_x_100.png";


    public Missile(int x, int y) {
        super(x,y,35,2,"main/res/Flame_01-100_x_100.png");
    }


    @Override
    public Missile clone() {        
        Missile ms1 = (Missile) super.clone();
        String e1 = "Dabartinio hash: " + this.hashCode();
        String e2 = "Naujo hash: " + ms1.hashCode();
//        Logger.getLogger(Missile.class.getName()).log(Level.OFF, e1);
//        Logger.getLogger(Missile.class.getName()).log(Level.OFF, e2);
        
        return ms1;
    }
}
