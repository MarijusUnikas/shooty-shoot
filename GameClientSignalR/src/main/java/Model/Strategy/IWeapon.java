package main.java.Model.Strategy;

import java.util.List;

public interface IWeapon {
    List<Missile> shoot(int x, int y);
}
