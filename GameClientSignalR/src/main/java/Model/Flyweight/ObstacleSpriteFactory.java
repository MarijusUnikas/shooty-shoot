package main.java.Model.Flyweight;

import java.util.HashMap;
import java.util.Map;

public class ObstacleSpriteFactory {
    public static Map<String, ISprite> sprites = new HashMap<>();    
    
    public static ISprite createSprite(String type){
        type = type.toLowerCase();
        ISprite sprite = sprites.get(type);
        
        if(sprite == null)
        {
            switch(type){
                case "blackhole":
                    sprite = new BlackholeSprite();
                    break;
                case "lightning":
                    sprite = new LightiningSprite();
                    break;
                case "meteorite":
                    sprite = new MeteoriteSprite();
                    break;
                case "rock":
                    sprite = new RockSprite();
                    break;                    
            }
            sprites.put(type, sprite);                
        }            
                      
        return sprite;
    }
}
