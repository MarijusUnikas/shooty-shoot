/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.Model.Flyweight;

import java.awt.Image;
import java.net.URL;
import javax.swing.ImageIcon;

/**
 *
 * @author Acer
 */
public class RockSprite implements ISprite {

    private Image image;

    public RockSprite() {
        loadImage();
    }
    
    @Override
    public Image getImage(){
        return image;
    }
    
    @Override    
    public void loadImage() {
        ClassLoader cl = getClass().getClassLoader();
        URL pathToImage = cl.getResource("main/res/Meteor_03_100x100.png");
        ImageIcon ii = new ImageIcon(pathToImage);
        image = ii.getImage();
    }    
    
}
