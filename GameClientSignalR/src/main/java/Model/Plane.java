package main.java.Model;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;

import main.java.Model.Decorator.*;
import main.java.Model.Strategy.*;
import main.java.Observer.MissileObserver;
import main.java.Observer.Observable;
import main.java.Observer.Observer;

// TODO: create MapObject
public class Plane implements Observable{
	
    @com.google.gson.annotations.SerializedName("PosX")
    private int x;
    @com.google.gson.annotations.SerializedName("PosY")
    private int y;
    @com.google.gson.annotations.SerializedName("HP")
    private int hp;
    @com.google.gson.annotations.SerializedName("Colour")
    private String colour;
    @com.google.gson.annotations.SerializedName("Player")
    private Player player;
    @com.google.gson.annotations.SerializedName("Missiles")
    private List<Weapon> missiles;
    private IWeapon weapon;

    private transient  List<Observer> observers;

    private transient  int dx;
    private transient  int dy;

    private transient  int width;
    private transient  int height;
    private transient  Image image;
    public transient boolean isVisible = true;
    private transient IBonus bonus;

    public Plane() {
        bonus = new NoBonus();
    	observers = new ArrayList();
    	missiles = new ArrayList();
    	weapon = new Bullet();
    }

    public void loadImage() {
        ClassLoader cl = getClass().getClassLoader();
        URL pathToImage = cl.getResource("main/res/Plane_01_"+colour+"-100_x_100.png");
        ImageIcon ii = new ImageIcon(pathToImage);
        image = ii.getImage();

        width = image.getWidth(null);
        height = image.getHeight(null);
    }

    public void setBonus(String extraBonus){
        switch(extraBonus){
            case "Movement":
                bonus = new MovementSpeed(bonus);
                break;

            case "Power":
                bonus = new ShootingForce(bonus);
                break;

            case "Immunity":
                bonus = new Immunity(bonus);
                break;

            case "Freeze":
                bonus = new FreezeEnemies(bonus);
                break;
        }
    }


    public Player getPlayer() {
        
        return player;
    }
    
    public void move() {
        
        x += dx;
        y += dy;

        checkBounds();
        checkForWeaponUpgrade();
        notifyObservers();
    }

    public void gotHit(int damage){
        hp -= damage;

        if(hp < 0) {
            player.lostLive();

            if(player.getLives() < 0){
                isVisible = false;
            }
            else {
                hp = 100;
            }
        }
    }

    private void checkBounds() {
        if(isVisible) {
            if (x < 1) {
                x = 1;
            }

            if (y < 1) {
                y = 1;
            }

            if (x > 700)
                x = 700;

            if (y > 550)
                y = 550;
        }
        else {
            x = -100;
            y = -100;
        }
    }

    public List<Weapon> getMissiles(){
        return missiles;
    }

    public void fire() {
        List<Missile> fromWeapon = weapon.shoot(x, y);
        for(Missile mis : fromWeapon){
            missiles.add(mis);
        }
    }

    private void checkForWeaponUpgrade(){
        long score = player.getScore();
        if(score > 1000 && score < 3000){
            setWeapon(new Triangle());
        }
        else if(score > 3000) {
            setWeapon(new Railgun());
        }
    }

    public void setWeapon(IWeapon weaponChange) {
        weapon = weaponChange;
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, 50, 50);
    }

    public void addScore(int score){
        player.addScore(score);
    }

    public void keyPressed(KeyEvent e) {

        int key = e.getKeyCode();

        if (key == KeyEvent.VK_SPACE) {
            fire();
        }
        if (key == KeyEvent.VK_LEFT) {
            dx = -bonus.speed();
        }

        if (key == KeyEvent.VK_RIGHT) {
            dx = bonus.speed();
        }

        if (key == KeyEvent.VK_UP) {
            dy = -bonus.speed();
        }

        if (key == KeyEvent.VK_DOWN) {
            dy = bonus.speed();
        }
    }

    public void keyReleased(KeyEvent e) {
        
        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT) {
            dx = 0;
        }

        if (key == KeyEvent.VK_RIGHT) {
            dx = 0;
        }

        if (key == KeyEvent.VK_UP) {
            dy = 0;
        }

        if (key == KeyEvent.VK_DOWN) {
            dy = 0;
        }
    }

	@Override
	public void addObserver(Observer o) {
		this.observers.add(o);

	}

	@Override
	public void removeObserver(Observer o) {
        this.observers.remove(o);

	}

	@Override
	public void notifyObservers() {
		for(Observer ob : observers)
		{
		    ob.update(this);
        }
	}

    public int getX() {

        return x;
    }

    public int getY() {

        return y;
    }

    public int getWidth() {

        return width;
    }

    public int getHeight() {

        return height;
    }

    public Image getImage() {

        return image;
    }

    public void setImage(Image imageSet) {

        image = imageSet;
    }

    public String getColour() {
        return colour;
    }

    public int getHp() {
        return hp;
    }

    public void setVisible(boolean b) {
        isVisible = b;
    }
}
