package main.java.Model.Decorator;

public class NoBonus implements IBonus{

    @Override
    public IBonus addBonus() {
        return this;
    }

    @Override
    public int speed() {
        return 2;
    }

    @Override
    public int power() {
        return 10;
    }
}
