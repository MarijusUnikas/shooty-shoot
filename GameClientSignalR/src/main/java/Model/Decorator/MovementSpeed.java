package main.java.Model.Decorator;

public class MovementSpeed extends BonusDecorator{
    public MovementSpeed(IBonus bonus) {
        super(bonus);
    }

    @Override
    public int speed() {
        return super.speed()*2;
    }
}
