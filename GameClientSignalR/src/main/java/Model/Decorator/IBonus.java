package main.java.Model.Decorator;

public interface IBonus {
    IBonus addBonus();
    int speed();
    int power();
}
