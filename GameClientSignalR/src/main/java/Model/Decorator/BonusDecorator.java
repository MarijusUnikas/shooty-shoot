package main.java.Model.Decorator;

public class BonusDecorator implements IBonus{
    IBonus bonus;
    public BonusDecorator(IBonus bonus){
        this.bonus = bonus;
    }

    @Override
    public IBonus addBonus() {
        return this;
    }

    @Override
    public int speed() {
        return bonus.speed();
    }

    @Override
    public int power() {
        return bonus.power();
    }
}
