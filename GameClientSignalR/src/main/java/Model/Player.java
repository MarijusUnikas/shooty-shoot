package main.java.Model;

public class Player {

    @com.google.gson.annotations.SerializedName("Name")
    private String name;
    
    @com.google.gson.annotations.SerializedName("Score")
    private long score;
    
    @com.google.gson.annotations.SerializedName("Lives")
    private int lives;
    
    @com.google.gson.annotations.SerializedName("IsReady")
    private boolean isReady;

    public Player() {
    }


    public String getName() {
        return name;
    }

    public boolean isReady() {
        return isReady;
    }

    public int getLives() {
        return lives;
    }

    public void lostLive() {
        lives--;
    }

    public long getScore() {
        return score;
    }

    public void addScore(int add) {
        score += add;
    }
}