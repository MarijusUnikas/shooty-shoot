package main.java.Model;

import main.java.GamePanel;
import main.java.Model.AbstractFactory.Enemy;
import main.java.Model.AbstractFactory.EnemyAbstractFactory;
import main.java.Model.AbstractFactory.EnemyPack1;
import main.java.Model.AbstractFactory.EnemyPack2;
import main.java.Model.Command.AttackCommand;
import main.java.Model.Command.AttackExecutor;
import main.java.Model.Command.ICommand;
import main.java.Model.Factory.Obstacle;
import main.java.Model.Factory.ObstacleCreator;
import main.java.Model.Flyweight.ObstacleSpriteFactory;
import main.java.Observer.MissileObserver;
import main.java.Observer.PlanePosObserver;

import java.awt.event.KeyEvent;
import java.util.*;

import main.java.Model.Facade.GameFacade;
import main.java.Model.Flyweight.ISprite;

public class Game implements GameFacade {

    @com.google.gson.annotations.SerializedName("Planes")
    private List<Plane> mPlanes;

    @com.google.gson.annotations.SerializedName("Enemies")
    private static List<Enemy> mEnemies;

    @com.google.gson.annotations.SerializedName("Obstacles")
    private static List<Obstacle> mObstacles;

    private static List<Crate> mCrates;

    @com.google.gson.annotations.SerializedName("GamePaused")
    private boolean GamePaused;

    private static transient final List<String> OBSTACLES = Arrays.asList("BLACKHOLE", "ROCK", "LIGHTNING", "METEORITE");
    private static transient ObstacleCreator obstacleCreator = new ObstacleCreator();
    private AttackExecutor attackExecutor = new AttackExecutor();

    static EnemyAbstractFactory pack1;
    static EnemyAbstractFactory pack2;

    public Game() {
        pack1 = new EnemyPack1();
        pack2 = new EnemyPack2();
        mEnemies = new ArrayList<Enemy>();
        mObstacles = new ArrayList<Obstacle>();
        mCrates = new ArrayList<Crate>();
    }

    public void setPlanes(List<Plane> planes)
    {
        mPlanes = planes;
    }

    public List<Plane> getPlanes()
    {
        return mPlanes;
    }

    public List<Enemy> getEnemies()
    {
        return mEnemies;
    }

    public void setObstacles(List<Obstacle> mObstacles) {
        this.mObstacles = mObstacles; }

    public List<Obstacle> getObstacles() { return mObstacles;}

    @Override
    public Plane init()
    {
        Plane myPlane = new Plane();
        for(Plane pl : mPlanes)
        {
            pl.addObserver(new PlanePosObserver());
            pl.addObserver(new MissileObserver());
            if(pl.getPlayer().getName().equals(GamePanel.myPlayerName)){
                myPlane = pl;
            }
        }

        return myPlane;
    }

    // TODO: To server side
    public void generateEnemies(String enemyType, int posY, String uid, boolean isFirstPack){
        Enemy en;

        if(isFirstPack) {
            switch (enemyType) {
                case "SpaceShip":
                    en = pack1.createSpaceShip();
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + enemyType);
            }

        }
        else {
            switch (enemyType) {
                case "SpaceShip":
                    en = pack2.createSpaceShip();
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + enemyType);
            }
        }
            en.setY(posY);
            en.setId(uid);
            mEnemies.add(en);
    }

    public void generateObstacle(String name, int posY, String uid){
        ISprite sprite = ObstacleSpriteFactory.createSprite(name);
        Obstacle obs = ObstacleCreator.createObstacle(name);        
        obs.setSprite(sprite);
        obs.setY(posY);
        mObstacles.add(obs);
    }

    public void removeEnemy(String uid) {
        for(Enemy en : mEnemies){
            if(en.getId().equals(uid))
                en.setVisible(false);
        }
    }

    @Override
    public void playerScored(int score, String playerName) {
        for(Plane pl : mPlanes)
        {
            if(pl.getPlayer().getName().equals(playerName)){
                pl.addScore(score);
            }
        }
    }
   
    @Override
    public void playerLost(String playerName) {
        for(Plane pl : mPlanes)
        {
            if(pl.getPlayer().getName().equals(playerName)){
                pl.setVisible(false);
            }
        }
    }

    public void generateBonus(String bonusName, int bonusY, String id) {
        Crate crate = new Crate(bonusName, bonusY);
        crate.setId(id);
        mCrates.add(crate);
    }

    // command pattern testing
    public void keyPressed(KeyEvent e) {

        int key = e.getKeyCode();

        if (key == KeyEvent.VK_P) {
            for(Enemy en : mEnemies){
                ICommand attackCommand = new AttackCommand(en);
                attackExecutor.executeOperation(attackCommand);
            }
        }
        if (key == KeyEvent.VK_O) {
            attackExecutor.undoOperation();
        }

    }

    public List<Crate> getCrates() {
        return mCrates;
    }

    public void removeCrate(String uid) {
        for(Crate en : mCrates){
            if(en.getId().equals(uid))
                en.setVisible(false);
        }
    }
}
