package main.java.Model.Singleton;

import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;
import main.java.GamePanel;
import main.java.Model.Factory.Obstacle;
import main.java.Model.Game;
import main.java.Model.Plane;
import main.java.Model.Player;

import java.util.Arrays;

public class ServerConnection {

    //SignalR variables
    private static String url = "http://localhost:5000/gamehub";
    private static Game gameObj = new Game();    // nes po facade igyvendinimo neleidzia static metodu naudot
    public  static HubConnection hubConnection;
    private static ServerConnection _instance;

    private ServerConnection()
    {
        initConnection();
    }

    public static ServerConnection getInstance()
    {
        ServerConnection localCon = _instance;
        if (localCon == null){
            synchronized (ServerConnection.class){
                localCon = _instance;
                if(localCon == null) {
                    _instance = localCon = new ServerConnection();
                }
            }
        }
        return localCon;
    }

    //Connection to SignalR Server
    private void initConnection(){

        hubConnection = HubConnectionBuilder.create(url).build();

        //Server Called functions
        //Must be declared before starting connection
        hubConnection.on("SetCurrentPlayer", (plane) -> {
            GamePanel.SetCurrentPlayer(plane);
        }, Plane.class);

        hubConnection.on("GetAllPlayers", (players) -> {
            GamePanel.GetAllPlayers(Arrays.asList(players));
        }, Player[].class);

        hubConnection.on("GetAllPlanes", (planes) -> {
            GamePanel.GetAllPlanes(Arrays.asList(planes));
        }, Plane[].class);

        hubConnection.on("generateObstacle", (name, posY, uid) -> {
           gameObj.generateObstacle(name, posY, uid);
        }, String.class, Integer.class, String.class);

        hubConnection.on("generateEnemies", (type, posY, uid, isFirstPack) -> {
            gameObj.generateEnemies(type, posY, uid, isFirstPack);
        }, String.class, Integer.class, String.class, Boolean.class);

        hubConnection.on("removeEnemy", (uid) -> {
            gameObj.removeEnemy(uid);
        }, String.class);

        hubConnection.on("updateObstacles", () ->{
            GamePanel.updateObstacles();
        });

        hubConnection.on("updateEnemies", () ->{
            GamePanel.updateEnemies();
        });

        hubConnection.on("updateCrates", () ->{
            GamePanel.updateCrates();
        });

        hubConnection.on("updateTimer", (elapsedTime) ->{
            GamePanel.updateTimer(elapsedTime);
        }, Integer.class);

        hubConnection.on("StartGame", (game) -> {
            GamePanel.StartGame(game);
        }, Game.class);

        hubConnection.on("playerLost", (playerName) ->{
            GamePanel.playerLost(playerName);
        }, String.class);

        hubConnection.on("playerScored", (score, playerName) ->{
            GamePanel.playerScored(score, playerName);
        }, Integer.class, String.class);

        hubConnection.on("generateBonus", (bonusName, bonusY, id) -> {
            gameObj.generateBonus(bonusName, bonusY, id);
        }, String.class, Integer.class, String.class);

        hubConnection.on("removeCrate", (uid) -> {
            gameObj.removeCrate(uid);
        }, String.class);

        hubConnection.start();
        System.out.println("connection established");
    }
}
