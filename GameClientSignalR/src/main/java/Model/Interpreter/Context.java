
package main.java.Model.Interpreter;

public class Context {
    String Input;

    public Context(String input)
    {
        this.Input = input;
    }    

    public String getInput() {
        return Input;
    }

    public void setInput(String Input) {
        this.Input = Input;
    }    
}
