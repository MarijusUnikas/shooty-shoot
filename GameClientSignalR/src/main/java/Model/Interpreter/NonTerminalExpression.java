
package main.java.Model.Interpreter;

import java.awt.Color;
import java.util.ArrayList;
import javax.swing.JFrame;
import main.java.GamePanel;

public class NonTerminalExpression extends AbstractExpression {
    private AbstractExpression expr1;
    private AbstractExpression expr2;
    private GamePanel panel;

    public NonTerminalExpression(AbstractExpression expr1, AbstractExpression expr2, GamePanel panel) {
        this.expr1 = expr1;
        this.expr2 = expr2;
        this.panel = panel;
    }

    @Override
    public boolean interpreter(String context) {
        
        context = context.toLowerCase();
        String[] parse = context.split("\\s+");
        if (parse.length == 3)
        {
            if (expr1.interpreter(parse[0]) && expr2.interpreter(parse[1]))
            {
                return execute(parse[2]);
            }
        }
        return false;
    }
    
    public boolean execute(String color)
    {
        boolean changed = true;

        switch (color)
        {
            case "red":
                panel.changeColor(Color.red);               
                break;
            case "black":
                panel.changeColor(Color.black);               
                break;                
            case "green":
                panel.changeColor(Color.green);               
                break;                
            case "gray":
                panel.changeColor(Color.gray);               
                break;                
            default:
                changed = false;
                break;                
        }
        return changed;
    }
}
