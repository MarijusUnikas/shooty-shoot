package main.java.Model.Interpreter;


public abstract class AbstractExpression {
    public abstract boolean interpreter(String context);
}
