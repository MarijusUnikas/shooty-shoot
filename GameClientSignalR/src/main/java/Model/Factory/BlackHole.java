package main.java.Model.Factory;

import javax.swing.*;
import java.awt.*;
import java.net.URL;
import java.util.UUID;
import main.java.Model.Flyweight.ISprite;

public class BlackHole implements Obstacle {


    @com.google.gson.annotations.SerializedName("PosX")
    private int x;
    @com.google.gson.annotations.SerializedName("PosY")
    private int y;
    @com.google.gson.annotations.SerializedName("HP")
    private int hp;
    @com.google.gson.annotations.SerializedName("GivenPoints")
    private int givenPoints;
    @com.google.gson.annotations.SerializedName("ID")
    private UUID id;
    public static final int DAMAGE = 2000;

    public ISprite extrinsicData;    

    private transient  int width;
    private transient  int height;
    private transient Image image;
    private transient final int OBSTACLE_SPEED = 10;
    protected transient boolean visible;

    public BlackHole() {
        this.x = BOARD_WIDTH;
        this.givenPoints = 0;
        this.hp = INDESTRUCTIBLE;
        this.id = UUID.randomUUID();
        visible = true;
        
    }

    @Override
    public Image getExtrinsicImage() {
        return extrinsicData.getImage();
    }
    
    @Override
    public ISprite getExtr(){
        return this.extrinsicData;
        
    }
    
    
    
    @Override
    public String getName(){
        return "blackhole";
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public int getDamage() {
        return DAMAGE;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public boolean isVisible() {
        return visible;
    }

    @Override
    public void setVisible(Boolean visible) {
        this.visible=visible;
    }

    @Override
    public void move() {
        x -= OBSTACLE_SPEED;
        if (x < OUTSIDE_THE_BOARD)
            visible = false;
    }

    @Override
    public void loadImage() {
        ClassLoader cl = getClass().getClassLoader();
        URL pathToImage = cl.getResource("main/res/blackhole50x50.png");
        ImageIcon ii = new ImageIcon(pathToImage);
        image = ii.getImage();
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle(x, y, 50, 50);
    }

    @Override
    public void updateHp(int damage){
        hp-=0;
    }

    @Override
    public int getHp() {
        return hp;
    }

    @Override
    public void setSprite(ISprite sprite) {
        this.extrinsicData = sprite;
    }
}
