/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.Model.Factory;

import java.util.ArrayList;
import main.java.Model.Flyweight.ISprite;
import main.java.Model.Flyweight.ObstacleSpriteFactory;

/**
 *
 * @author Acer
 */
public class Demo {

    public static ArrayList<Obstacle> obs = new ArrayList<>();    
    public static int n = 100;
    
    public static void main(String[] args) {
        
//        testRegular();
        testFlyweight();
        

    }
    
    public static void testRegular(){
  
        long startTime = System.currentTimeMillis();
        long beforeUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();		        
        
        for (int i = 0; i < n; i++) {
            Obstacle o = ObstacleCreator.createObstacle("meteorite");
            o.setY(45);
            o.loadImage();
            obs.add(o);
        }                 
        
        long stopTime = System.currentTimeMillis();        
        long afterUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();				         
        long KB = (afterUsedMem - beforeUsedMem) >> 10;
        long MB = KB >> 10; 
        System.out.println("Regular factory method memory utilized: " + (afterUsedMem - beforeUsedMem) + " bytes | " + MB + " MB");        
        System.out.println("Time: " + (stopTime - startTime));        
    }
    
    public static void testFlyweight(){
        ISprite sprite;
        long startTime = System.currentTimeMillis();
        long beforeUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();		
        
        
        for (int i = 0; i < n; i++) {
            Obstacle o = ObstacleCreator.createObstacle("meteorite");
            o.setY(45);
            sprite = ObstacleSpriteFactory.createSprite("meteorite");
            o.setSprite(sprite);
            obs.add(o);
        }
        
        long stopTime = System.currentTimeMillis();        
        long afterUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();				         
        long KB = (afterUsedMem - beforeUsedMem) >> 10;
        long MB = KB >> 10; 
        System.out.println("Flyweight method memory utilized: " + (afterUsedMem - beforeUsedMem) + " bytes | " + MB + " MB");        
        System.out.println("Time: " + (stopTime - startTime));                
    }
    
}
