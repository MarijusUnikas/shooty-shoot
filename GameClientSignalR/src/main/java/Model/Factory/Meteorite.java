package main.java.Model.Factory;

import javax.swing.*;
import java.awt.*;
import java.net.URL;
import java.util.UUID;
import main.java.Model.Flyweight.ISprite;

public class Meteorite  implements Obstacle {
    @com.google.gson.annotations.SerializedName("PosX")
    private int x;
    @com.google.gson.annotations.SerializedName("PosY")
    private int y;
    @com.google.gson.annotations.SerializedName("HP")
    private int hp;
    @com.google.gson.annotations.SerializedName("GivenPoints")
    private int givenPoints;
    @com.google.gson.annotations.SerializedName("ID")
    private UUID id;
    public static final int DAMAGE = 10;

    private transient  int width;
    private transient  int height;
    private transient Image image;
    protected transient boolean visible;
    public ISprite extrinsicData;    
    
    
    public Meteorite() {

        this.x = BOARD_WIDTH;
        this.givenPoints = 10;
        this.hp = 40;
        this.id = UUID.randomUUID();
        visible = true;
        this.extrinsicData = null;
    }
    
     @Override
    public ISprite getExtr(){
        return this.extrinsicData;
        
    }
    
        @Override
    public Image getExtrinsicImage() {
        return extrinsicData.getImage();
    }
    
    @Override
    public String getName(){
        return "meteorite";
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public int getDamage() {
        return DAMAGE;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public boolean isVisible() {
        return visible;
    }

    @Override
    public void setVisible(Boolean visible) {
        this.visible=visible;
    }

    @Override
    public void move() {
        x -= OBSTACLE_SPEED;
        if (x < OUTSIDE_THE_BOARD)
            visible = false;
    }

    @Override
    public void loadImage() {
        ClassLoader cl = getClass().getClassLoader();
        URL pathToImage = cl.getResource("main/res/Meteor_01_50x50.png");
        ImageIcon ii = new ImageIcon(pathToImage);
        image = ii.getImage();
    }

    @Override
    public void updateHp(int damage){
        hp-=damage;
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle(x, y, 50, 50);
    }

    @Override
    public int getHp() {
        return hp;
    }
    
    @Override
    public void setSprite(ISprite sprite) {
         this.extrinsicData = sprite;
    }
}
