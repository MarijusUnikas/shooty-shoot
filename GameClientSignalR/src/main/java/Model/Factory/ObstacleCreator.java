package main.java.Model.Factory;

import java.util.HashMap;
import java.util.Map;

public class ObstacleCreator {
    

    public static Obstacle createObstacle(String type){
        
        if (type == null){
            return null;
        }
                
        if (type.equalsIgnoreCase("BLACKHOLE")){
            return new BlackHole();
        }
        if (type.equalsIgnoreCase("METEORITE")){
            return new Meteorite();
        }
        if (type.equalsIgnoreCase("ROCK")){
            return new Rock();
        }
        if (type.equalsIgnoreCase("LIGHTNING")){
            return new Lightining();
        }
            return null;
    }
}
