package main.java.Model.Factory;

import java.awt.*;
import main.java.Model.Flyweight.ISprite;

public interface Obstacle {
    public static final int OBSTACLE_SPEED = 1;
    public static final int BOARD_WIDTH = 800;
    public static final int OUTSIDE_THE_BOARD = -50;
    public static final int INDESTRUCTIBLE = 5000;

    public int getX();

    public int getY();

    public int getDamage();

    public void setX(int x);

    public void setY(int y);

    public Image getImage();

    public boolean isVisible();

    public void setVisible(Boolean visible);

    public void move();

    public void loadImage();

    public Rectangle getBounds();

    public void updateHp(int damage);

    public int getHp();
    
    public String getName();
    
    public Image getExtrinsicImage();
    public ISprite getExtr();
    public void setSprite(ISprite sprite);
}
