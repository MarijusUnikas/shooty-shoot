package main.java;

import java.awt.*;

import javax.swing.JFrame;

public class GameClient extends JFrame{
	
	public GameClient() {
		initUI();
	}

	
    public static void main(String[] args) {

        EventQueue.invokeLater(() -> {
            GameClient ex= new GameClient();
            ex.setVisible(true);
        });
    }
    
    private void initUI() {
        add(new GamePanel());
        setTitle("Shooty-shoot");
        setSize(800, 700);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
    }
}
