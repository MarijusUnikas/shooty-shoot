package main.java;

import java.awt.*;
import java.awt.event.*;
import java.net.URL;
import java.util.List;
import java.util.TimerTask;

import javax.swing.*;

import main.java.Model.AbstractFactory.Enemy;
import main.java.Model.Crate;
import main.java.Model.Factory.Meteorite;
import main.java.Model.Factory.Obstacle;
import main.java.Model.Game;
import main.java.Model.Plane;
import main.java.Model.Player;
import main.java.Model.Singleton.ServerConnection;
import main.java.Model.Strategy.Missile;
import main.java.Model.Strategy.Weapon;

import java.util.logging.Level;
import java.util.logging.Logger;
import main.java.Model.Flyweight.ObstacleSpriteFactory;
import main.java.Model.Interpreter.AbstractExpression;
import main.java.Model.Interpreter.Context;
import main.java.Model.Interpreter.NonTerminalExpression;
import main.java.Model.Interpreter.TerminalExpression;

//TODO: Fix this mess
public class GamePanel extends JPanel implements Runnable, KeyListener {

    private static ServerConnection serverCon;
    public static String myPlayerName;

    static Player currentPlayer;
    static List<Player> mPlayers;
    static JFrame lobby;
    static JPanel panel = new JPanel();      
    static Game _game;
    private boolean doDrawScoreBoard = false;

    private static Timer timer;
    private static java.util.Timer genTimer;
    private static Plane mPlane;
    private final int DELAY = 10;
    private static boolean doGenerate = true;
    private static boolean doGenerateObstacles = true;
    private static int timeElapsed = 0;

    private Thread thread;
    private boolean running;
    private int FPS = 60;
    private long targetTime = 1000 / FPS;

    public GamePanel() {
        super();
        serverCon = serverCon.getInstance();
        initBoard();
        PlayerNameForm();
        setLayout(new FlowLayout());        
        consoleWindow();       
    }
    
    public void consoleWindow(){
        panel.setPreferredSize(new Dimension(800, 40));
        panel.setLocation(0,750);    
        
        panel.setBackground(Color.gray);  
        JLabel result = new JLabel("");             
        JLabel b1 = new JLabel("Write command: ");     
        JTextField textField = new JTextField("", 15);                 
        JButton execCmdButton = new JButton(">>");
        JButton closeButton = new JButton("CLOSE");                
        
        b1.setBounds(150,150,20,20);    
        b1.setBackground(Color.yellow);   
         
        
        execCmdButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                System.out.println("The entered text is: " + textField.getText());
                
                if(executeCommand(textField.getText()))
                    result.setText("Done..");
                else
                    result.setText("Bad command");

                textField.setText("");
            }
        });
        
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                result.setText("");
                panel.setVisible(false);
            }
        });
                        
      panel.add(b1);  
      panel.add(textField,"South");                        
      panel.add(execCmdButton);                 
      panel.add(closeButton);                       
      panel.add(result);                             
      panel.setVisible(false);      
      add(panel);                  
    }
    
    public boolean executeCommand(String commandInput) {

        Context context = new Context(commandInput);
        AbstractExpression background = new TerminalExpression("set");
        AbstractExpression operation = new TerminalExpression("background");        
        if(context.getInput().length() == 0){
            System.out.println("Bad command");            
        }
        else
        {
            AbstractExpression command = new NonTerminalExpression(background, operation, this);
            if(command.interpreter(context.getInput())) {
                return true;
            }
        }
        
        return false;
    }
   
    public void changeColor(Color c) {
        setBackground(c);
    }

    public static void playerLost(String playerName) {
        _game.playerLost(playerName);
    }

    public static void playerScored(int score, String playerName) {
        _game.playerScored(score, playerName);
    }

    private void initBoard() {
        setBackground(Color.black);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
    }

    private void updatePlanes() {
        if(mPlane != null) {
            mPlane.move();
        }
    }

    public static void updateTimer(int elapsed) {
        timeElapsed = elapsed;
    }

    private void updateMissiles() {
        if(mPlane == null)
            return;

        List<Weapon> ms = mPlane.getMissiles();

        for (int i = 0; i < ms.size(); i++) {

            Weapon m = ms.get(i);

            if (m.isVisible()) {
                m.move();
            } else {
                ms.remove(i);
            }
        }
    }

    // TODO: To server side
    public static void updateEnemies() {
        List<Enemy> enemies = _game.getEnemies();

        for (int i = 0; i < enemies.size(); i++) {

            Enemy en = enemies.get(i);

            if (en.isVisible()) {
                en.move();
            } else {
                enemies.remove(en);
            }
        }
    }

    public static void updateObstacles() {
        List<Obstacle> obs = _game.getObstacles();
        for (int i = 0; i < obs.size(); i++) {
            Obstacle o = obs.get(i);
            if (o.isVisible()) {
                o.move();
            } else {
                obs.remove(i);
            }
        }
    }

    public static void updateCrates() {
        List<Crate> crates = _game.getCrates();
        for (int i = 0; i < crates.size(); i++) {
            Crate c = crates.get(i);
            if (c.isVisible()) {
                c.move();
            } else {
                crates.remove(i);
            }
        }
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        doDrawing(g);
        
        Toolkit.getDefaultToolkit().sync();
    }
    
    
    private void paintObject(Graphics2D g, int posX, int posY, String location){
        Image img;
        ClassLoader cl = getClass().getClassLoader();
        URL pathToImage = cl.getResource(location);
        ImageIcon ii = new ImageIcon(pathToImage);
        img = ii.getImage();
        g.drawImage(img, posX, posY, this);    
    }

    private void doDrawing(Graphics g) {
        
        Graphics2D g2d = (Graphics2D) g;
        if(_game != null) {
            for (Plane pl : _game.getPlanes()){
                if(pl.isVisible) {
                    // TODO: find a better way to load images
                    pl.loadImage();
                    g2d.drawImage(pl.getImage(), pl.getX(),
                            pl.getY(), this);

                    //Missiles are drawn here
                    List<Weapon> ms = pl.getMissiles();
                    for (Weapon missile : ms) {
                        this.paintObject(g2d, missile.getX(), missile.getY(), "main/res/Flame_01-100_x_100.png");  
                    }
                }
            }

            for(Enemy en : _game.getEnemies()){
                g2d.drawImage(en.getImage(), en.getX(),
                        en.getY(), this);
            }

            // Obstacles are drawn here
            List<Obstacle> obs = _game.getObstacles();
            for (Obstacle obstacle : obs) {                
                g2d.drawImage(obstacle.getExtrinsicImage(), obstacle.getX(),
                        obstacle.getY(), this);
            }
                       
            // Crates are drawn here
            List<Crate> crates = _game.getCrates();
            for (Crate crate : crates) {
//                System.out.println(crate.getBonusName());
                g2d.drawImage(crate.getImage(), crate.getX(),
                        crate.getY(), this);
            }
            // TODO: Move to game class
            int minutes = (timeElapsed / 60) % 60;
            int seconds = timeElapsed % 60;

            g.setColor(Color.WHITE);
            // TODO: Add image of HP
            g.drawString("Lives left: " + mPlane.getPlayer().getLives(), 5, 15);
            g.drawString("HP: " + mPlane.getHp(), 100, 15);
            g.drawString( String.format("%02d:%02d", minutes, seconds), 350, 15);
            g.drawString("Score: " + mPlane.getPlayer().getScore(), 600, 15);

            // TODO: Make game over image
            if(!mPlane.isVisible){
                g.setColor(Color.RED);
                g.drawString("GAME OVER", 350, 350);
            }

            if(doDrawScoreBoard || !mPlane.isVisible){
                drawScoreBoard(g);
            }
        }
    }

    private void drawScoreBoard(Graphics g){
        Graphics2D g2d = (Graphics2D) g;
        g.setColor(Color.WHITE);
        g.drawString("Players score:", 350, 80);

        int offset = 20;
        List<Plane> planes = _game.getPlanes();

        for(int i = 0; i < planes.size(); i++){
            g.drawString(planes.get(i).getPlayer().getName() +
                    " " + planes.get(i).getPlayer().getScore(), 350, 100+offset*i);
        }
    }

    public void addNotify() {
        super.addNotify();
        if(thread == null) {
            thread = new Thread(this);
            addKeyListener(this);
            thread.start();
        }
    }

    public void run() {
        running = true;

        long start;
        long elapsed;
        long wait;

        // game loop
        while(running) {

            start = System.nanoTime();

            if(mPlane!= null) {
                updatePlanes();
                updateMissiles();
                //updateEnemies();
                //updateObstacles();
                checkCollisions();

                repaint();
            }

            elapsed = System.nanoTime() - start;

            wait = targetTime - elapsed / 1000000;
            if(wait < 0) wait = 5;

            try {
                Thread.sleep(wait);
            }
            catch(Exception e) {
                e.printStackTrace();
            }

        }
    }

    public void checkCollisions() {

        Rectangle r3 = mPlane.getBounds();

        //Plane collision with enemies
        for (Enemy en : _game.getEnemies()) {

            Rectangle r2 = en.getBounds();

            if (r3.intersects(r2)) {

                mPlane.gotHit(10);
                serverCon.hubConnection.send("RemoveEnemy", en.getId());                
                en.setVisible(false);

            }
        }
        // checks obstacle collisions with a plane
        for (Obstacle obs : _game.getObstacles()) {
            Rectangle r2 = obs.getBounds();
            if (r3.intersects(r2)) {
                mPlane.gotHit(obs.getDamage());
            }
        }

        // checks crates collisions with a plane
        for (Crate crate : _game.getCrates()) {
            Rectangle r2 = crate.getBounds();
            if (r3.intersects(r2)) {
                mPlane.setBonus(crate.getBonusName());
                serverCon.hubConnection.send("RemoveCrate", crate.getId());
                crate.setVisible(false);
            }
        }

        if(!mPlane.isVisible)
        {
            serverCon.hubConnection.send("PlayerLostAllLives");
        }

        List<Weapon> ms = mPlane.getMissiles();

        //Missile collision with enemies
        for (Weapon m : ms) {

            Rectangle r1 = m.getBounds();

            for (Enemy en : _game.getEnemies()) {

                Rectangle r2 = en.getBounds();

                if (r1.intersects(r2)) {
                    en.gotHit(20);
                    m.setVisible(false);                    
                    if(en.getHp() <= 0)
                    {
                        mPlane.getPlayer().addScore(35);
                        serverCon.hubConnection.send("RemoveEnemy", en.getId());
                        serverCon.hubConnection.send("PlayerScored", 35);
                        en.setVisible(false);
                    }
                }
            }
            // checks missile collisions with obstacles
            for (Obstacle obs : _game.getObstacles()) {
                Rectangle r2 = obs.getBounds();
                if (r1.intersects(r2)) {
                    mPlane.getPlayer().addScore(35);
                    serverCon.hubConnection.send("PlayerScored", 35);
                    m.setVisible(false);
                    if (obs.getHp() < 0)
                        obs.setVisible(false);
                }
            }
        }
    }

    public void PlayerNameForm() {
        myPlayerName = JOptionPane.showInputDialog(null, "Your player name",
                "Player name", JOptionPane.PLAIN_MESSAGE);
        RegisterPlayer(myPlayerName);
        DrawLobby();
    }

    public static void StartGame(Game game) {
        lobby.dispatchEvent(new WindowEvent(lobby,WindowEvent.WINDOW_CLOSING));
        _game = game;
        mPlane = _game.init();
    }

    public static void SetCurrentPlayer(Plane plane) {
        currentPlayer = plane.getPlayer();
    }

    public static void GetAllPlayers(List<Player> players) {
        mPlayers = players;
        DrawPlayersInLobby();
    }

    public static void GetAllPlanes(List<Plane> planes) {
       _game.setPlanes(planes);
    }

    public static void GetAllObstacles(List<Obstacle> obstacles) { _game.setObstacles(obstacles);}

    // public static void AddObstacle(int x, int y, int hp, int givenPoints) { hubConnection.send("AddObstacle", x, y, hp, givenPoints);}

    public static void RegisterPlayer(String myPlayerName) {
        serverCon.hubConnection.send("AddUser", myPlayerName);
    }

    public static void DrawPlayersInLobby() {
        lobby.getContentPane().removeAll();
        for (Player pl : mPlayers) {
            JInternalFrame in = new JInternalFrame(pl.getName(), true, false, false, false);

            JButton button = new JButton("Ready!");
            button.setBackground(Color.RED);
            if(pl.isReady())
            {
                button.setBackground(Color.GREEN);
            }
            if(currentPlayer != null)
            {
                if(!pl.getName().equals(currentPlayer.getName())){
                    button.setEnabled(false);
                }
            }
            button.addActionListener(e -> {
                serverCon.hubConnection.send("TogglePlayerReady");
            });

            JLabel l = new JLabel("                                        ");
            JPanel p = new JPanel();
            p.add(l);
            p.add(button);
            in.setVisible(true);
            in.add(p);

            lobby.add(in);
            lobby.repaint();
        }
    }

    public static void DrawLobby() {

        lobby = new JFrame();
        lobby.setLayout(new FlowLayout());
        lobby.setSize(400, 500);
        lobby.setLocationRelativeTo(null);
        lobby.setVisible(true);
        lobby.setAlwaysOnTop(true);

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        mPlane.keyPressed(e);
        _game.keyPressed(e); // for command pattern testing

       if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
           panel.setVisible(true);
        }
        
        if (e.getKeyCode() == KeyEvent.VK_TAB) {
            doDrawScoreBoard = true;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        mPlane.keyReleased(e);

        if (e.getKeyCode() == KeyEvent.VK_TAB) {
            doDrawScoreBoard = false;
        }
    }
}
