package main.java.Observer;

import main.java.GamePanel;
import main.java.Model.Plane;
import main.java.Model.Singleton.ServerConnection;

public class PlanePosObserver implements Observer {

    @Override
    public void update(Object o) {
        Plane a = (Plane) o;
        ServerConnection.getInstance().
                hubConnection.send("UpdatePlanePos", a.getX(), a.getY());
    }
}
