package main.java.Observer;

public interface Observer {
	public void update(Object o);
}
