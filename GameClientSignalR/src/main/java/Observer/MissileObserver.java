package main.java.Observer;

import main.java.GamePanel;
import main.java.Model.Plane;
import main.java.Model.Singleton.ServerConnection;
import main.java.Model.Strategy.Weapon;

import java.util.List;

public class MissileObserver implements Observer {
    @Override
    public void update(Object o) {
        Plane p = (Plane) o;
        List<Weapon> pMissiles = p.getMissiles();
        if(!pMissiles.isEmpty()) {
            ServerConnection.getInstance()
            .hubConnection.send("UpdateMissiles", pMissiles);
        }
    }
}
